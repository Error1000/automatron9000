@echo off

REM This is to fix some wierdness with varaibles inside ifs
setlocal EnableDelayedExpansion


set "false=n"
set "true=y"


set "libs="
set "targetOs="
set "specifiedos=!false!"
set "specifiedlibs=!false!"
set "tomake="
set "specifiedtomake=!false!"
set "specifiedmakefile=!false!"
set "makefile=makefile"

REM This iterates thrugh all the options/arguments
REM ------------------------------------------------------------------------------------------------------------------------------------
:GETOPTS 

REM This parses the wierd way options work in windows
REM option = the actual option so "-l", or "-m", or "--help"
REM OPTARG = all the args of the option separated by a space
REM -------------------------------------------------------------------

 set "option=%1"
 set "exitLoop=!false!"
 set "OPTARG="
 REM This is here to make sure that the arguments of this option aren't options, if they are options skip the argument ghatering and treat everything approriatley
 REM --------------------------------
  echo %2|findstr /r "^-.*$" >nul 2>&1
  if not errorlevel 1 ( 
    set "OPTARG=" 
	REM Note setting exit loop isn't necessary but i like to have it anyways jic(just in case) i happen to use some wierd logic after the endloop that uses this variable
    set "exitLoop=!true!"
    goto LoopEnd 
  )
 REM --------------------------------

 :LoopStart 
    set "cond=!false!"
    REM This checks to see if the current option is not a new argument
    echo %3|findstr /r "^-.*$" >nul 2>&1
    if not errorlevel 1 set "cond=!true!"
    if "%3"=="" set "cond=!true!"
    if "!cond!"=="!true!" set "exitLoop=!true!"
    
	REM This is here to remove the first space chracter that is accidentally added to the OPTARG variable
    if "%OPTARG%"=="" (
	 set "OPTARG=%2" 
	) else ( 
	 set "OPTARG=%OPTARG% %2"
    )
	
	SHIFT
    if "!exitLoop!"=="!false!" goto LoopStart 
 :LoopEnd 


REM -------------------------------------------------------------------


if "%option%"=="-l" (
  set "libs=!OPTARG!"
  set "specifiedlibs=!true!"
)

if "%option%"=="-m" (
  set "tomake=!OPTARG!"
  set "specifiedtomake=!true!"
)

if "%option%"=="-o" (
 set "targetOs=!OPTARG!"
 set "specifiedos=!true!"
)

if "%option%"=="-f" (
 set "makefile=!OPTARG!"
 set "specifiedmakefile=!true!"
)

REM This will shift the options given to the command so that now we are reading the next option
SHIFT 

if not "%2"=="" goto GETOPTS 
REM ------------------------------------------------------------------------------------------------------------------------------------

if "%specifiedtomake%"=="!false!" (
  echo Please choose to make at least something!
  goto :EOF
)

if "%specifiedos%"=="!false!" (
 if "%specifiedlibs%"=="!true!" (
  echo OS hasn't been choosen, not installing libraries!!
 )
)



echo %targetOs%|findstr /r "windows" >nul 2>&1
if not errorlevel 1 (
 REM This installs chocolatey if it's not installed
 choco -v >nul 2>&1 || echo Installing chocolatey, please be patient, this may take a while ... && @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin" 
 
 REM DO NOT USE refreshenv HERE WHEN THE IF WILL EXIT IT WILL TERMINATE EXECUTION FOR SOME REASON!!!!!
 REM I know, i know allow-empty-checksums really shouldn't be here but it's not that bad and it protects againt any problems on choco's side, do i think it's ok
  cmd.exe /c "refreshenv & choco install --yes --accept-license --allow-empty-checksums mingw"

 REM This will install libraries
 if "%specifiedlibs%"=="!true!" (
  REM Make sure 7-zip is installed
  cmd.exe /c  "C:\ProgramData\chocolatey\bin\RefreshEnv.cmd & choco install --yes --accept-license --allow-empty-checksums 7zip"
  REM Maybe some day i could use this cmd.exe /c  "refreshenv & choco install --yes --accept-license --allow-empty-checksums %libs%"
  for %%a in (%libs%) do (
    cmd.exe /c "C:\ProgramData\chocolatey\bin\RefreshEnv.cmd & cd %CD% & cd Libraries/windows/ & 7z.exe x -y %%a"
  )
 )
 
 if "%specifiedmakefile%"=="!false!" (
  set "makefile=makefile-windows"
 )
)


if "%specifiedmakefile%"=="!false!" (
  echo Makefile name was not specified using default guessed filename: "%makefile%"!!
)


if "%specifiedtomake%"=="%true%" (
   cmd.exe /c "C:\ProgramData\chocolatey\bin\RefreshEnv.cmd & cd %CD% & mingw32-make -f %makefile% %tomake%"
)




if not errorlevel 0 (
 echo Return code was not zero for make!
 goto :EOF
) else (
 echo Done^?
 goto :EOF
)

REM This is here just because i want to make sure it's defined, no other particular reason, plus most implmentations should allow this
:EOF
endlocal